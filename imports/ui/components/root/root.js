import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import { name as Navigation } from '../navigation/navigation';
import { name as Music } from '../music/music';
import { name as Booking } from '../booking/booking';
import { name as Contact } from '../contact/contact';
import { name as Home } from '../home/home';

import './root.html'

class Root{}

const name = 'root';

export default angular.module(name, [
  angularMeteor,
  uiRouter,
  Navigation,
  Booking,
  Music,
  Contact,
  Home
])
  .component(name, {
    templateUrl: `imports/ui/components/${name}/${name}.html`,
    controllerAs: name,
    controller: Root
})
  .config(config);

function config($locationProvider, $urlRouterProvider) {
  'ngInject';

  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/home');
}
