import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import template from './music.html';

class Music{
  constructor($stateParams) {
    'ngInject';
  }
}

const name = "music";

export default angular.module(name, [
  angularMeteor,
  uiRouter
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  controllerAs: name,
  controller: Music
})
  .config(config);

function config($stateProvider) {
  'ngInject';

  $stateProvider.state('mixes', {
      url:'/mixes',
      template: '<music></music>'
    });
}
