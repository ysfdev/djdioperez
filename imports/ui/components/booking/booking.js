import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import template from './booking.html';

class Booking{
  constructor($stateParams) {
    'ngInject';
  }
}

const name = "booking";

export default angular.module(name, [
  angularMeteor,
  uiRouter
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  controllerAs: name,
  controller: Booking
})
  .config(config);

function config($stateProvider) {
  'ngInject';

  $stateProvider.state('booking', {
      url:'/booking',
      template: '<booking></booking>'
    });
}
